﻿// Steck.final.v.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <stack>
using namespace std;

int main()
{
    setlocale(LC_ALL, "rus");
    stack <int> steck;
    int size;
    cout << "Введите размерность стека"<<endl;
    cin >> size;
    cout << "Заполните стек"<<endl;
    int i = 0;

    while (i != size) 
    {
        int a;
        cin >> a;

        steck.push(a);
        i++;
    }
    if (steck.empty()) cout << "Стек не пуст"<<endl;
    cout << "Верхний элемент стека: " << steck.top() << endl;
    cout << "Давайте удалим верхний элемент " << endl;
    steck.pop();
    int newnom;
    cout << "Введите новое число"<<endl;
    cin >> newnom;
    steck.push(newnom);
    if (steck.empty()) cout << "Стек не пуст" << endl;
    cout << "Новая вершина стека = " << steck.top() << endl;
    system("pause");
    return 0;
}
